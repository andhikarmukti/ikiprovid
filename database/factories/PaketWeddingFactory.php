<?php

namespace Database\Factories;

use App\Models\PaketWedding;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaketWeddingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PaketWedding::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
