<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <title>Form</title>
</head>
<body>
    <form>
        <div class="container">
            <div class="row mt-2 justify-content-md-center">
                <div class="col-6">
                    <div class="card mt-5">
                        <div class="card-body">
                                <h4 class="text-center mb-5">Paket Wedding</h4>
                                <div class="mb-3">
                                <small for="exampleInputEmail1" class="form-label">Paket Wedding</small>
                                <select class="form-select" aria-label="Default select example" id="paketWedding">
                                    <option selected disabled>Silahkan pilih paket</option>
                                    <option value="Basic">Basic</option>
                                    <option value="Premium">Premium</option>
                                    <option value="VIP">VIP</option>
                                </select>
                                {{-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> --}}
                                </div>
                                <div class="card p-2">
                                    <div class="mb-3">
                                        <small for="exampleInputPassword1" class="form-label" id="demoTemplateLabel">Basic</small>
                                        <select class="form-select" aria-label="Default select example" id="demoTemplate">
                                            <option selected disabled>Silahkan pilih demo</option>
                                        </select>
                                    </div>

                                    <div class="mb-3">
                                    <small for="urlDomain" class="form-label">URL Domain</small>
                                    <select class="form-select" aria-label="Default select example" id="urlDomain">
                                        <option selected disabled>Pilih URL</option>
                                        <option value="momenspesial.my.id">momenspesial.my.id</option>
                                        <option value="undang.pw">undang.pw</option>
                                        <option value="haribahagia.pw">haribahagia.pw</option>
                                    </select>
                                    </div>

                                    <div class="mb-3">
                                    <small for="urlDomain" class="form-label">Nama Sub Domain</small>
                                    <input type="text" class="form-control" placeholder="contoh : (bambang-heni.momenspesial.my.id) atau (udin-prisilia.undang.pw)">
                                    </div>

                                    <div class="mb-3">
                                    <small for="requestMusic" class="form-label">Request Music</small>
                                    <input type="text" id="requestMusic" name="requestMusic" class="form-control" placeholder="Sertakan link jika ada">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                

                <div class="row mt-2 justify-content-md-center">
                    <div class="col-6">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h4 class="text-center mb-5">Data Mempelai Pria</h4>

                                <div class="card p-2 mb-3">
                                    <div class="d-flex">
                                        <div class="mb-3 col-5">
                                            <small for="namaMempelaiPria" class="form-label">Nama Lengkap Mempelai Pria</small>
                                            <input type="text" class="form-control" name="namaMempelaiPria" id="namaMempelaiPria">
                                        </div>
                                        <div class="col-2"></div>
                                        <div class="mb-3 col-5">
                                            <small for="namaPanggilanPria" class="form-label">Nama Panggilan Pria</small>
                                            <input type="text" class="form-control" name="namaPanggilanPria" id="namaPanggilanPria">
                                        </div>
                                    </div>
                                </div>

                                <div class="card p-2">
                                    <div class="d-flex">
                                        <div class="mb-3 col-5">
                                            <small for="putraKe" class="form-label">Putra Ke-</small>
                                            <input type="text" class="form-control" name="putraKe" id="putraKe" placeholder="isi dengan angka saja">
                                        </div>
                                        <div class="col-2"></div>
                                        <div class="mb-3 col-5">
                                            <small for="instagramPria" class="form-label">Instagram Mempelai Pria</small>
                                            <input type="text" class="form-control" name="instagramPria" id="instagramPria">
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-center">
                                    <div class="mb-3 col-5">
                                        <small for="formFileLg" class="form-label d-flex justify-content-center mt-4">Foto Mempelai Pria</small>
                                        <input class="form-control form-control-md" id="formFileLg" type="file">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2 justify-content-md-center">
                    <div class="col-6">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h4 class="text-center mb-5">Data Mempelai Wanita</h4>

                                <div class="card p-2 mb-3">
                                    <div class="d-flex">
                                        <div class="mb-3 col-5">
                                            <small for="namaMempelaiWanita" class="form-label">Nama Lengkap Mempelai Wanita</small>
                                            <input type="text" class="form-control" name="namaMempelaiWanita" id="namaMempelaiWanita">
                                        </div>
                                        <div class="col-2"></div>
                                        <div class="mb-3 col-5">
                                            <small for="namaPanggilanWanita" class="form-label">Nama Panggilan Wanita</small>
                                            <input type="text" class="form-control" name="namaPanggilanWanita" id="namaPanggilanWanita">
                                        </div>
                                    </div>
                                </div>

                                <div class="card p-2">
                                    <div class="d-flex">
                                        <div class="mb-3 col-5">
                                            <small for="putraKe" class="form-label">Putri Ke-</small>
                                            <input type="text" class="form-control" name="putraKe" id="putraKe" placeholder="isi dengan angka saja">
                                        </div>
                                        <div class="col-2"></div>
                                        <div class="mb-3 col-5">
                                            <small for="instagramWanita" class="form-label">Instagram Mempelai Wanita</small>
                                            <input type="text" class="form-control" name="instagramWanita" id="instagramWanita">
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-center">
                                    <div class="mb-3 col-5">
                                        <small for="formFileLg" class="form-label d-flex justify-content-center mt-4">Foto Mempelai Wanita</small>
                                        <input class="form-control form-control-md" id="formFileLg" type="file">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2 justify-content-md-center">
                    <div class="col-6">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h4 class="text-center mb-5">Data Orang Tua Mempelai Pria</h4>

                                <div class="card p-2 mb-3">
                                    <div class="d-flex">
                                        <div class="mb-3 col-5">
                                            <small for="namaAyahMempelaiPria" class="form-label">Nama Ayah</small>
                                            <input type="text" class="form-control" name="namaAyahMempelaiPria" id="namaAyahMempelaiPria">
                                        </div>
                                        <div class="col-2"></div>
                                        <div class="mb-3 col-5">
                                            <small for="namaIbuMempelaiPria" class="form-label">Nama Ibu</small>
                                            <input type="text" class="form-control" name="namaIbuMempelaiPria" id="namaIbuMempelaiPria">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2 justify-content-md-center">
                    <div class="col-6">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h4 class="text-center mb-5">Data Orang Tua Mempelai Wanita</h4>

                                <div class="card p-2 mb-3">
                                    <div class="d-flex">
                                        <div class="mb-3 col-5">
                                            <small for="namaAyahMempelaiWanita" class="form-label">Nama Ayah</small>
                                            <input type="text" class="form-control" name="namaAyahMempelaiWanita" id="namaAyahMempelaiWanita">
                                        </div>
                                        <div class="col-2"></div>
                                        <div class="mb-3 col-5">
                                            <small for="namaIbuMempelaiWanita" class="form-label">Nama Ibu</small>
                                            <input type="text" class="form-control" name="namaIbuMempelaiWanita" id="namaIbuMempelaiWanita">
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2 justify-content-md-center">
                    <div class="col-6">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h4 class="text-center mb-5">Form Detail Acara</h4>

                                <div class="card p-2 mb-3">
                                    <div class="d-flex">
                                        <div class="mb-3 col-5">
                                            <small for="tanggalAkad" class="form-label">Tanggal Akad / Pemberkatan</small>
                                            <input type="date" class="form-control" name="tanggalAkad" id="tanggalAkad">
                                        </div>
                                        <div class="col-2"></div>
                                        <div class="mb-3 col-5">
                                            <small for="jamAkad" class="form-label">Jam Akad / Pemberkatan</small>
                                            <input type="time" class="form-control" name="jamAkad" id="jamAkad">
                                        </div>
                                    </div>
                                </div>

                                <div class="card p-2 mb-3">
                                    <div class="d-flex">
                                        <div class="mb-3 col-5">
                                            <small for="tanggalResepsi" class="form-label">Tanggal Resepsi</small>
                                            <input type="date" class="form-control" name="tanggalResepsi" id="tanggalResepsi">
                                        </div>
                                        <div class="col-2"></div>
                                        <div class="mb-3 col-5">
                                            <small for="jamResepsi" class="form-label">Jam Resepsi</small>
                                            <input type="time" class="form-control" name="jamResepsi" id="jamResepsi">
                                        </div>
                                    </div>
                                </div>

                                <div class="card p-2 mb-3">
                                    <div class="d-flex">
                                        <div class="mb-3 col-5">
                                            <small for="alamatAkad" class="form-label">Alamat Akad</small>
                                            <input type="date" class="form-control" name="alamatAkad" id="alamatAkad">
                                        </div>
                                        <div class="col-2"></div>
                                        <div class="mb-3 col-5">
                                            <small for="alamatResepsi" class="form-label">Alamat Resepsi</small>
                                            <input type="time" class="form-control" name="alamatResepsi" id="alamatResepsi">
                                        </div>
                                    </div>
                                </div>

                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="ayatSuciDanQuotes" class="form-label d-flex justify-content-center">Ayat Suci & Quotes</small>
                                        <textarea class="form-control" name="ayatSuciDanQuotes" id="ayatSuciDanQuotes"></textarea>
                                    </div>
                                </div>

                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="catatanLain" class="form-label d-flex justify-content-center">Catatan Lain Yang Tidak Disebutkan Diatas</small>
                                        <textarea class="form-control" name="catatanLain" id="catatanLain"></textarea>
                                    </div>
                                </div>

                                <input type="hidden" value="Checkus" id="rekanan" name="rekanan">
                                
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-4">Submit</button>
                    </div>
                </div>

            </div>
        </div>
    </form>
    
    






      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

      {{-- Jquery --}}
      <script>
          $('#paketWedding').change(function(){
              var paket = $(this).val();
            $('#demoTemplateLabel').text(paket)
            $.ajax({
                type : 'GET',
                dataType : 'JSON',
                url : '/form',
                data :{
                    paket : paket
                },
                success : function(result){
                    console.log(result)
                    $('#demoTemplate').empty();
                    $('#demoTemplate').append(`
                        <option selected disabled>Silahkan pilih demo</option>
                    `)
                    $.each(result, function(index, element){
                    $('#demoTemplate').append(`
                        <option value="`+ element +`" id="demoTemplateOption">`+ element +`</option>
                    `)
                    })
                }
            })
          });
      </script>
</body>
</html>